extends StaticBody2D

signal open_lane


export (int) var id


func _on_Button_button_up() -> void:
	emit_signal("open_lane", id)
