extends KinematicBody2D


func _physics_process(delta: float) -> void:
	var velocity = Vector2()
	
	velocity.y += 6000 * delta
	
	velocity = move_and_slide(velocity)


func _on_VisibilityNotifier2D_screen_exited():
	self.queue_free()
