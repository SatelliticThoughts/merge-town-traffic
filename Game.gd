extends Node2D


onready var lights = [	get_node("Light"),
						get_node("Light2"),
						get_node("Light3"),
						get_node("Light4"),
						get_node("Light5"),
						get_node("Light6")]

onready var spawners = [get_node("Spawner"),
						get_node("Spawner2"),
						get_node("Spawner3"),
						get_node("Spawner4"),
						get_node("Spawner5"),
						get_node("Spawner6")]

onready var _lbl_score = get_node("Score")

var _active: int = 0
var _score: int = 0
var _stage: int = 10


func _ready() -> void:
	for light in lights:
		light.connect("open_lane", self, "_on_open_lane")
	for spawner in spawners:
		spawner.connect("vehicle_removed", self, "_update_score")

func _physics_process(delta) -> void:
	for spawner in spawners:
		if spawner.get_body_counter() > 10:
			print("Game Over")

func _on_open_lane(id) -> void:
	add_child(lights[_active])
	_active = id
	remove_child(lights[id])

func _update_score() -> void:
	_score += 1
	_lbl_score.set_text(str(_score))
	if _score > _stage:
		_stage += _stage
		for spawner in spawners:
			spawner.set_random_rate(spawner.get_random_rate() - 20)
